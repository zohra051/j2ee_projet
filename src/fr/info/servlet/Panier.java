package fr.info.servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet("/Panier")
public class Panier extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public Panier() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute("nom_prod",request.getSession().getAttribute("nom"));
		request.setAttribute("prix_prod",request.getSession().getAttribute("prix"));
		this.getServletContext().getRequestDispatcher("/panier.jsp").forward(request, response);
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		ArrayList<String> liste_nom =(ArrayList<String>) session.getAttribute("nom");
		ArrayList<String> liste_prix =(ArrayList<String>)session.getAttribute("prix");
		ArrayList<Integer> liste_id =(ArrayList<Integer>)session.getAttribute("id");
		
		liste_nom.clear();
		liste_prix.clear();
		liste_id.clear();
		
		doGet(request,response);
	}

}
