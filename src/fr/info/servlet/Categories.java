package fr.info.servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.info.bdd.CategoriesBDD;
import fr.info.bdd.ProduitBDD;
import fr.info.beans.CategoriesBean;
import fr.info.beans.ProduitBean;

@WebServlet("/Categories")
public class Categories extends HttpServlet {
	private static final long serialVersionUID = 1L;
    ArrayList<String> nom_liste_produit = new ArrayList<>();
    ArrayList<String> prix_liste_produit = new ArrayList<>();
    ArrayList<Integer> id_list_produit = new ArrayList<>();

	
    public Categories() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		CategoriesBDD categorie = new CategoriesBDD();
		ProduitBDD produit = new ProduitBDD();
		ArrayList<CategoriesBean> liste_categorie = categorie.recupererCategorie();
		ArrayList<ProduitBean> produits = new ArrayList<ProduitBean>();
		String selection = request.getParameter("select");
		for(int i=0; i< liste_categorie.size();i++) {
			if(selection != null && selection.equals(liste_categorie.get(i).getNom()))
			{
				produits = produit.recupererProduit(selection);		
			}
			
		}
		request.setAttribute("categories",liste_categorie);
		request.setAttribute("produits", produits);
		
		this.getServletContext().getRequestDispatcher("/categories.jsp").forward(request, response);	
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request,response);
		String nom_du_produit = request.getParameter("nom_produit");
		String prix_du_produit = request.getParameter("prix_produit");
		int id_produit = Integer.parseInt(request.getParameter("id_produit"));
		//int id = Integer.valueOf(id_du_produit);
		nom_liste_produit.add(nom_du_produit);
		prix_liste_produit.add(prix_du_produit);
		id_list_produit.add(id_produit);
		for(int i=0;i<nom_liste_produit.size();i++) {
			System.out.println(nom_liste_produit.get(i));
			System.out.println(prix_liste_produit.get(i));
		}
		
		HttpSession session = request.getSession();
	    session.setAttribute("nom", nom_liste_produit);
	    session.setAttribute("prix", prix_liste_produit);
	    session.setAttribute("id",id_list_produit);
		
	}

}
