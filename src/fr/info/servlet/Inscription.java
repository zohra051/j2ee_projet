package fr.info.servlet;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.info.bdd.CommandeBDD;
import fr.info.bdd.InscriptionBDD;
import fr.info.bdd.ProduitCommandeBDD;
import fr.info.beans.CommandeBean;
import fr.info.beans.InscriptionBean;
import fr.info.beans.ProduitCommandeBean;

/**
 * Servlet implementation class Inscription
 */
@WebServlet("/Inscription")
public class Inscription extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
 
    public Inscription() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.getServletContext().getRequestDispatcher("/inscription.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		/*VERIFICATION*/
		String nom_du_client = request.getParameter("user_name");
		String prenom_du_client = request.getParameter("user_surname");
		String email_du_client = request.getParameter("user_mail");
		String rue_du_client = request.getParameter("user_street");
		String cp_du_client = request.getParameter("user_cp");
		String ville_du_client = request.getParameter("user_city");
		String pays_du_client = request.getParameter("user_country");
		String tel_du_client = request.getParameter("user_number");
		String mdp_du_client = request.getParameter("user_password");
		
		
		
		if( !nom_du_client.equals("") &&
				!prenom_du_client.equals("") &&
				!email_du_client.equals("") &&
				!rue_du_client.equals("") &&
				!cp_du_client.equals("") &&
				!ville_du_client.equals("") &&
				!pays_du_client.equals("") &&
				!tel_du_client.equals("") &&
				!mdp_du_client.equals("") )
		{
			/*ECRIRE DANS LA BASE*/
			InscriptionBean clientBean = new InscriptionBean();
			InscriptionBDD  clientBDD = new InscriptionBDD();
			
			int code_postal_du_client = Integer.parseInt(cp_du_client);
			int telephone_du_client = Integer.parseInt(tel_du_client);
			
			clientBean.setNom(nom_du_client);
			clientBean.setPrenom(prenom_du_client);
			clientBean.setEmail(email_du_client);
			clientBean.setRue(rue_du_client);
			clientBean.setCode_postal(code_postal_du_client);
			clientBean.setVille(ville_du_client);
			clientBean.setPays(pays_du_client);
			clientBean.setTelephone(telephone_du_client);
			clientBean.setMot_de_passe(mdp_du_client);
			
			
			clientBDD.envoyerInscription(clientBean);
			
			/*création de la commande*/
			
			 double randomDouble = Math.random();
				randomDouble = randomDouble * 100000 + 1;
				int randomInt = (int) randomDouble;
				
				/*************/
			HttpSession session = request.getSession();
			float total = (float) session.getAttribute("total");
		    
			LocalDate date = LocalDate.now();
		    
		    clientBean = clientBDD.recupererId(clientBean);
		    		    
		    CommandeBean commande = new CommandeBean();
		    commande.setTotal(total);
		    commande.setDate_creation(date);
		    commande.setId_client(clientBean.getId());
		    commande.setNo_commande(randomInt);
		    
		    CommandeBDD commandeBDD = new CommandeBDD();
		    commandeBDD.insererCommande(commande);
		    
		    commande = commandeBDD.recupererId(commande);
		    
		    session.setAttribute("numCommande", commande.getNo_commande());
		    
		    /*Insertion de ProduitCommande*/
		    ArrayList<Integer> liste_id = (ArrayList<Integer>) session.getAttribute("id");
		    
		    ArrayList<ProduitCommandeBean> liste_produit_commande = new ArrayList<>();
		    for(int i=0;i<liste_id.size();i++)
		    {
		    	System.out.println(commande.getId_commande());
		    	ProduitCommandeBean bean = new ProduitCommandeBean();
		    	bean.setCommande_id(commande.getId_commande());
		    	bean.setProduit_id(liste_id.get(i));
		    	bean.setQuantite(1);
		    	liste_produit_commande.add(bean);
		    }
		    
		    ProduitCommandeBDD produitCommandeBDD = new ProduitCommandeBDD();
		    for(int i=0;i<liste_produit_commande.size();i++)
		    {
		    	produitCommandeBDD.insererProduitCommande(liste_produit_commande.get(i));
		    }
		    
			/*Passage à la page suivante*/
			response.sendRedirect("/boutique/panier/espace_utilisateur/final");
		}
		else
		{
			doGet(request,response);
		}
		
		
	}

}
