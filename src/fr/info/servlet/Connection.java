package fr.info.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.info.bdd.ConnectionBDD;

/**
 * Servlet implementation class Connection
 */
@WebServlet("/Connection")
public class Connection extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public Connection() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.getServletContext().getRequestDispatcher("/connection.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/*Récupération du mail et mdp*/
		String email = request.getParameter("user_login");
		String mdp = request.getParameter("user_mdpco");
		System.out.println(email);
		System.out.println(mdp);
		ConnectionBDD connectionBDD = new ConnectionBDD();
		if(connectionBDD.comparerConnection(email, mdp))
		{
			response.sendRedirect("/boutique/panier/espace_utilisateur/connection/adresse");
			/*envoie du mail et mdp*/
			HttpSession session = request.getSession();
		    session.setAttribute("email", email);
		    session.setAttribute("mdp", mdp);
		}
		else
		{
			doGet(request,response);
		}
	}

}
