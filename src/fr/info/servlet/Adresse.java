package fr.info.servlet;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.info.bdd.AdresseBDD;
import fr.info.bdd.CommandeBDD;
import fr.info.bdd.InscriptionBDD;
import fr.info.bdd.ProduitCommandeBDD;
import fr.info.beans.CommandeBean;
import fr.info.beans.InscriptionBean;
import fr.info.beans.ProduitCommandeBean;

/**
 * Servlet implementation class Adresse
 */
@WebServlet("/Adresse")
public class Adresse extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Adresse() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.getServletContext().getRequestDispatcher("/adresse.jsp").forward(request, response);
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		/*VERIFICATION*/
		String rue_du_client = request.getParameter("user_street_co");
		String cp_du_client = request.getParameter("user_cp_co");
		String ville_du_client = request.getParameter("user_city_co");
		String pays_du_client = request.getParameter("user_country_co");
		
		
		
		if( 	!rue_du_client.equals("") &&
				!cp_du_client.equals("") &&
				!ville_du_client.equals("") &&
				!pays_du_client.equals(""))
		{
			/*ECRIRE DANS LA BASE*/
			InscriptionBean clientBean = new InscriptionBean();
			AdresseBDD  clientBDD = new AdresseBDD();
			
			int code_postal_du_client = Integer.parseInt(cp_du_client);
						
			clientBean.setRue(rue_du_client);
			clientBean.setCode_postal(code_postal_du_client);
			clientBean.setVille(ville_du_client);
			clientBean.setPays(pays_du_client);
			
			HttpSession session = request.getSession();
		    String email = (String) session.getAttribute("email");
		    String mdp = (String) session.getAttribute("mdp");
			
		    clientBean.setEmail(email);
		    clientBean.setMot_de_passe(mdp);
		    
			clientBDD.modifierAdresse(clientBean);
			
			/*Insertion de la commande dans la base de donné */
			InscriptionBDD inscription = new InscriptionBDD();
			clientBean = inscription.recupererId(clientBean);
			
			CommandeBean commande = new CommandeBean();
			
			float total = (float) session.getAttribute("total");
		    
			LocalDate date = LocalDate.now();
			
			double randomDouble = Math.random();
			randomDouble = randomDouble * 100000 + 1;
			int randomInt = (int) randomDouble;
			
			commande.setTotal(total);
		    commande.setDate_creation(date);
		    commande.setId_client(clientBean.getId());
		    commande.setNo_commande(randomInt);
		    
		    CommandeBDD commandeBDD = new CommandeBDD();
		    commandeBDD.insererCommande(commande);
		    commande = commandeBDD.recupererId(commande);
		    
		    session.setAttribute("numCommande", commande.getNo_commande());
		    
		    /*Insertion de ProduitCommande*/
		    ArrayList<Integer> liste_id = (ArrayList<Integer>) session.getAttribute("id");
		    
		    ArrayList<ProduitCommandeBean> liste_produit_commande = new ArrayList<>();
		    for(int i=0;i<liste_id.size();i++)
		    {
		    	System.out.println(commande.getId_commande());
		    	ProduitCommandeBean bean = new ProduitCommandeBean();
		    	bean.setCommande_id(commande.getId_commande());
		    	bean.setProduit_id(liste_id.get(i));
		    	bean.setQuantite(1);
		    	liste_produit_commande.add(bean);
		    }
		    
		    ProduitCommandeBDD produitCommandeBDD = new ProduitCommandeBDD();
		    for(int i=0;i<liste_produit_commande.size();i++)
		    {
		    	produitCommandeBDD.insererProduitCommande(liste_produit_commande.get(i));
		    }
		    
		    
			
			/*Passage à la page suivante*/
			response.sendRedirect("/boutique/panier/espace_utilisateur/final");
			
		}
		else
		{
			doGet(request,response);
		}

		
		
	}

}
