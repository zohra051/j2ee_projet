package fr.info.beans;

import java.sql.Date;

public class ProduitBean {
	
		private int id;
		private String nom;
		private float prix;
		private String description;
		private Date dernier_maj;
		private int categorie_id;
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public String getNom() {
			return nom;
		}
		public void setNom(String nom) {
			this.nom = nom;
		}
		public float getPrix() {
			return prix;
		}
		public void setPrix(float prix) {
			this.prix = prix;
		}
		public String getDescription() {
			return description;
		}
		public void setDescription(String description) {
			this.description = description;
		}
		public Date getDernier_maj() {
			return dernier_maj;
		}
		public void setDernier_maj(Date dernier_maj) {
			this.dernier_maj = dernier_maj;
		}
		public int getCategorie_id() {
			return categorie_id;
		}
		public void setCategorie_id(int categorie_id) {
			this.categorie_id = categorie_id;
		}		
}

