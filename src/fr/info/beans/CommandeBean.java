package fr.info.beans;

import java.time.LocalDate;
import java.util.Date;

public class CommandeBean {
	
	private int id_commande;
	private float total;
	private LocalDate date_creation;
	private int no_commande;
	private int id_client;
	public int getId_commande() {
		return id_commande;
	}
	public void setId_commande(int id_commande) {
		this.id_commande = id_commande;
	}
	public float getTotal() {
		return total;
	}
	public void setTotal(float total) {
		this.total = total;
	}
	public LocalDate getDate_creation() {
		return date_creation;
	}
	public void setDate_creation(LocalDate date_creation) {
		this.date_creation = date_creation;
	}
	public int getNo_commande() {
		return no_commande;
	}
	public void setNo_commande(int no_commande) {
		this.no_commande = no_commande;
	}
	public int getId_client() {
		return id_client;
	}
	public void setId_client(int id_client) {
		this.id_client = id_client;
	}
	
	
}
