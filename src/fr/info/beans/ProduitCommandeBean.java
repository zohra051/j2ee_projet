package fr.info.beans;

public class ProduitCommandeBean {
	
	private int commande_id;
	private int produit_id;
	private int quantite;
	
	public ProduitCommandeBean() {}
	
	public int getCommande_id() {
		return commande_id;
	}
	public void setCommande_id(int commande_id) {
		this.commande_id = commande_id;
	}
	public int getProduit_id() {
		return produit_id;
	}
	public void setProduit_id(int produit_id) {
		this.produit_id = produit_id;
	}
	public int getQuantite() {
		return quantite;
	}
	public void setQuantite(int quantite) {
		this.quantite = quantite;
	}	
	
}
