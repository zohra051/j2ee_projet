package fr.info.bdd;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import fr.info.beans.CategoriesBean;

public class CategoriesBDD {
	
	public CategoriesBDD() {}
	

	public ArrayList<CategoriesBean> recupererCategorie(){
		
		ArrayList<CategoriesBean> list = new ArrayList<>();
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection connexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/catalog","jdenis","29011997");
			Statement statement = connexion.createStatement();
			ResultSet rs = statement.executeQuery("SELECT * from Categorie");
			while(rs.next()) {
				int id = rs.getInt("id_categorie");
				String nom = rs.getString("nom_categorie");
				CategoriesBean cat = new CategoriesBean();
				cat.setId(id);
				cat.setNom(nom);
				list.add(cat);
			}
			rs.close();
			statement.close();
			connexion.close();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return list;
	}


}
