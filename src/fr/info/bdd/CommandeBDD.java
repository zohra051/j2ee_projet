package fr.info.bdd;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import fr.info.beans.CommandeBean;
import fr.info.beans.InscriptionBean;

public class CommandeBDD {
	public CommandeBDD() {}
	
	public void insererCommande(CommandeBean commande)
	{
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection connexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/catalog","jdenis","29011997");
			PreparedStatement statement = connexion.prepareStatement("insert into Commande_Client (montant,date_creation,no_confirmation,client_id) values (?,?,?,?)");
			statement.setFloat(1,commande.getTotal());
			
			statement.setDate(2,java.sql.Date.valueOf(commande.getDate_creation()));
			statement.setInt(3,commande.getNo_commande());
			statement.setInt(4,commande.getId_client());
			statement.executeUpdate();
			statement.close();
			connexion.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public CommandeBean recupererId(CommandeBean commande)
	{
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection connexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/catalog","jdenis","29011997");
			PreparedStatement prepared = connexion.prepareStatement("select id_commande_client from Commande_Client where no_confirmation = ?");
	        prepared.setInt(1,commande.getNo_commande());
	        ResultSet rs = prepared.executeQuery();
	        if(rs.next())
	        {
	        	int id = rs.getInt("id_commande_client");
	        	commande.setId_commande(id);
	        }	        
	        prepared.close();
	        connexion.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return commande;
	}
}
