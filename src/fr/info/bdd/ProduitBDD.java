package fr.info.bdd;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import fr.info.beans.ProduitBean;


public class ProduitBDD {
	
	public ProduitBDD() {}
	
	public ArrayList<ProduitBean> recupererProduit(String categorie){
		ArrayList<ProduitBean> liste = new ArrayList<>();
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection connexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/catalog","jdenis","29011997");
			Statement statement = connexion.createStatement();
			ResultSet rs = statement.executeQuery("SELECT id_produit,nom_produit,prix_produit,description_produit,dernier_maj,categorie_id from Produit p INNER JOIN "
					+ "Categorie c ON p.categorie_id = c.id_categorie "
					+ "WHERE c.nom_categorie = \""+categorie+"\"");
			while(rs.next()) {
				int id = rs.getInt("id_produit");
				String nom = rs.getString("nom_produit");
				float prix = rs.getFloat("prix_produit");
				String description = rs.getString("description_produit");
				Date date = rs.getDate("dernier_maj");
				int id_cat = rs.getInt("categorie_id");
				ProduitBean prod = new ProduitBean();
				prod.setId(id);
				prod.setNom(nom);
				prod.setPrix(prix);
				prod.setDescription(description);
				prod.setDernier_maj(date);
				prod.setCategorie_id(id_cat);
				liste.add(prod);
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return liste;
	}
}
