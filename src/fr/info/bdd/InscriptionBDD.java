package fr.info.bdd;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.*;
import fr.info.beans.InscriptionBean;

public class InscriptionBDD {

	public InscriptionBDD(){}
	
	public void envoyerInscription(InscriptionBean client)  		 
	{
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection connexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/catalog","jdenis","29011997");
		
	        PreparedStatement prepared = connexion.prepareStatement("INSERT INTO Client (nom_client,prenom_client,email,rue,code_postal,ville,pays,telephone,mot_de_passe) VALUES (?,?,?,?,?,?,?,?,?)");
	        prepared.setString(1,client.getNom());
	        prepared.setString(2,client.getPrenom());
	        prepared.setString(3,client.getEmail());
	        prepared.setString(4,client.getRue());
	        prepared.setInt(5,client.getCode_postal());
	        prepared.setString(6,client.getVille());
	        prepared.setString(7,client.getPays());
	        prepared.setInt(8,client.getTelephone());
	        prepared.setString(9,client.getMot_de_passe());
	        prepared.executeUpdate();
	        
	        prepared.close();
	        connexion.close();
	        
	       
		
	 } catch (Exception e) {
         e.printStackTrace();
     }		
	}
	
	public InscriptionBean recupererId(InscriptionBean client)
	{
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection connexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/catalog","jdenis","29011997");
			PreparedStatement prepared = connexion.prepareStatement("select id_client from Client where email = ? and mot_de_passe = ?");
	        prepared.setString(1,client.getEmail());
	        prepared.setString(2,client.getMot_de_passe());
	        ResultSet rs = prepared.executeQuery();
	        if(rs.next())
	        {
	        	int id = rs.getInt("id_client");
	        	client.setId(id);
	        }	        
	        prepared.close();
	        connexion.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return client;
	}
}
