package fr.info.bdd;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

import fr.info.beans.ProduitCommandeBean;

public class ProduitCommandeBDD {
	public ProduitCommandeBDD(){}
	
	public void insererProduitCommande(ProduitCommandeBean produit) {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection connexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/catalog","jdenis","29011997");
			PreparedStatement statement = connexion.prepareStatement("insert into Produit_Commande (commande_client_id,produit_id,quantite) values (?,?,?)");
			statement.setInt(1,produit.getCommande_id());
			statement.setInt(2,produit.getProduit_id());
			statement.setInt(3,produit.getQuantite());
			statement.executeUpdate();
			statement.close();
			connexion.close();
	}catch(Exception e) {
		e.printStackTrace();
	}
}
}
