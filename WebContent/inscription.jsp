<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	 <link rel="stylesheet" href="${pageContext.request.contextPath}/style/style.css" />
        <link rel="stylesheet" href="${pageContext.request.contextPath}/style/connection_ou_inscription.css" />
         <link rel="stylesheet" href="${pageContext.request.contextPath}/style/client.css" />
        <link rel="stylesheet" href="${pageContext.request.contextPath}/style/panier.css" />
        <title>Ma boutique en ligne</title>
</head>
<body>
	<%@ include file="template.jsp" %>
	<%@ include file="contenuInscription.jsp" %>
</body>
</html>