<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>


		<div class="contenu">

			<%@ include file="Menu.jsp" %>


			<div class="accueil text_blanc " >

				<h1 class="text_milieu_blanc"> Insription </h1>
				<center>
				<div class="inscription" >


				<form action="" method="post" >

				    <div class="en_ligne" >
						<label class="champ" for="name" placeholder="Harrat">Nom :</label>
				        <input  class="champ" type="text" id="name" name="user_name">
				    </div>
				    <div class="en_ligne">
				        <label class="champ" for="surname" placeholder="Zohra">Prénom :</label>
				        <input class="champ" type="text" id="surname" name="user_surname">
				    </div>
				    <div class="en_ligne">
				        <label class="champ" for="mail" placeholder="senouci12@hotmail.fr">E-mail :</label>
				        <input  class="champ" type="email" id="mail" name="user_mail">
				    </div >
				    	<div class="en_ligne">
				        <label class="champ" for="street" placeholder="1 rue Général de Gaulle">Rue :</label>
				        <input class="champ" type="text" id="street" name="user_street">
				    </div>
				    <div class="en_ligne">
				        <label class="champ"for="cp" placeholder="59760">Code Postal :</label>
				        <input class="champ" type="text" id="cp" name="user_cp">
				    </div>
				    <div class="en_ligne">
				        <label class="champ" for="city" placeholder="Grande-Synthe">Ville :</label>
				        <input class="champ" type="text" id="city" name="user_city" size="5">
				    </div>
				    <div class="en_ligne">
				        <label class="champ" for="country" placeholder="France">Pays :</label>
				        <input class="champ" type="text" id="country" name="user_country">
				    </div>
				    <div class="en_ligne">
				        <label class="champ" for="number" placeholder="0601020304">Téléphone :</label>
				        <input  class="champ" type="text" id="number" name="user_number" size="10">
				    </div>
				    <div class="en_ligne">
				        <label class="champ" for="password">Mot de passe :</label>
				        <input class="champ" type="text" id="password" name="user_password" min="8">
				    </div>
				    
				    <div >
						<input type="submit" value="Envoyer" class="button2 button3 " href="/boutique/panier/espace_utilisateur/final" />
					</div>
				</form>

				</div>

</center>
			</div>

		</div>