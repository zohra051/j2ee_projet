<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>acceuil</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/style/style.css" />
</head>
<body>
	<%@ include file="template.jsp" %>
	<%@ include file="contenuIndex.jsp" %>
</body>
</html>