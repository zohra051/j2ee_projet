<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
	<meta charset="utf-8" />
	 <link rel="stylesheet" href="${pageContext.request.contextPath}/style/style.css" />
        <title>Ma boutique en ligne</title>
	</head>
<body>
	<%@ include file="template.jsp" %>
	<%@ include file="contenuFinal.jsp" %>
</body>
</html>