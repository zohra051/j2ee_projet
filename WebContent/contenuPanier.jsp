<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ page import="java.util.ArrayList"   %>
<!DOCTYPE html>
		<div class="contenu">

			<%@ include file="Menu.jsp" %>

			<div class="accueil">

				<h2 class="menu_text"> Votre panier </h2>
				<div class="article_prix">
					<div class="article">
						<p class="text_milieu_blanc">Article</p>
					</div>
					<div class="prix">
						<p class="text_milieu_blanc"> Prix</p>
					</div>
				</div>
				<%
				ArrayList<String> liste_nom_panier = (ArrayList) request.getAttribute("nom_prod");
				ArrayList<String> liste_prix_panier = (ArrayList) request.getAttribute("prix_prod");
			
				float total=0;
				if(liste_prix_panier != null && liste_nom_panier != null){
					for(int i=0;i<liste_nom_panier.size();i++){
						out.println("<div class=\"article_prix\">");
						out.println("<div class=\"article\">");
							out.println("<p class=\"text_milieu_blanc\">"+liste_nom_panier.get(i)+"</p>");
						out.println("</div>");
						out.println("<div class=\"prix\">");
							out.println("<p class=\"text_milieu_blanc\">"+liste_prix_panier.get(i)+"</p>");
						out.println("</div>");
					out.println("</div>");
					total = total + Float.parseFloat(liste_prix_panier.get(i));
					}
				}
				
				HttpSession session_total = request.getSession();
			    session_total.setAttribute("total", total);
					
				%>
			
			<div class="article_prix">
					<div class="article">
						<p class="text_milieu_blanc" >TOTAL</p>
					</div>
					<div class="prix">
					<%
						String total_prix = String. valueOf(total);
						out.println("<p class=\"text_milieu_blanc\" style=\"color:red\">"+total_prix+"</p>");
					%>
						
					</div>
				</div>
				<%
					if(liste_prix_panier != null && liste_prix_panier.size() >0  && liste_nom_panier!= null && liste_nom_panier.size() > 0)
					{
						out.println("<div class=\"ajouter_produit  pad_left\" >");
						out.println("<button class=\"button4 button5\"><a href=\"/boutique/panier/espace_utilisateur\">Valider la commande</a></button>");
						out.println("</div>");
					}
				%>
				
				<div class="ajouter_produit  pad_left" style="display:flex">
					<form action="" method="post" >
						<input class="button4 button5" type="submit"  value="Vider le panier"/>
					</form>
					<a  href="/boutique/categories"> <p class="button4 button5">Continuer vos achats </p></a>
				</div>

			</div>

		</div>