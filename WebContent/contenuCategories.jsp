<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ page import="java.util.ArrayList" %>
<%@ page import="fr.info.beans.CategoriesBean" %>
<%@ page import="fr.info.beans.ProduitBean" %>
<!DOCTYPE html>
<div class="contenu">

			<%@ include file="Menu.jsp" %>

			<div class="accueil_C accueil">

				<div class="categorie_accueil" >

					<h1 class="menu_text"> Catégories </h1>
					
				<%
				ArrayList<CategoriesBean> liste = (ArrayList) request.getAttribute("categories");
				for(int i=0;i<liste.size();i++)
				{
					out.println("<a href=\"categories?select="+liste.get(i).getNom()+"\">");
					out.println("<p class=\"menu_text\">");
					out.println(liste.get(i).getNom());
					out.println("</p></a>");
				}
			%>

				</div>
				<div class="text_categorie">
					
					<%
						String selection = request.getParameter("select");
					
					ArrayList<ProduitBean> liste_produit = (ArrayList) request.getAttribute("produits");

					
						if(selection == null)
						{
							out.println("<h2 class=\"menu_text\"> Sélectionnez une catégorie de produit </h2>");
						}
						else{
							out.println("<h2 class=\"menu_text\"> "+ selection+" </h2>");
							for(int i=0;i<liste_produit.size();i++)
							{
							out.println("<div class=\"nouvelle_element\">");
							out.println("<form method=\"post\" action=\"\">");
								out.println("<div style=\"display:flex\">");
									out.println("<div class=\"text_du_produit\">");
										out.println("<input type=\"HIDDEN\" name=\"nom_produit\" value=\""+liste_produit.get(i).getNom()+"\"/>"+"<p  class=\"align_gauche menu_text\">"+liste_produit.get(i).getNom()+ "</p>");
									out.println("</div>");
									out.println("<div>");
										out.println("<input type=\"HIDDEN\" name=\"prix_produit\" value=\""+liste_produit.get(i).getPrix()+"\"/>"+"<p  class=\"prix menu_text\">"+liste_produit.get(i).getPrix()+ "</p>");
									out.println("</div>");
									out.println("<div class=\"ajouter_produit  pad_top_righ\" >");
										out.println("<input type=\"HIDDEN\" name=\"id_produit\" value=\""+liste_produit.get(i).getId()+"\"/>");
										out.println("<input type=\"submit\" value=\"Ajouter au panier\" class=\"button4 button5\" />");
									out.println("</div>");
								out.println("</div>");
								out.println("<div style=\"display:flex\">");
									out.println("<p style=\"color:white\">" +liste_produit.get(i).getDescription() +"</p>");
								out.println("</div>");
								out.println("</form>");
							out.println("</div>");
							}
							
						}
						
					%>
					

				</div>

			</div>

		</div>