DROP TABLE Produit_Commande;
DROP TABLE Produit;
DROP TABLE Categorie;
DROP TABLE Commande_Client;
DROP TABLE Client;


CREATE Database catalog;

use catalog;

CREATE TABLE Categorie (
	id_categorie INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
	nom_categorie VARCHAR(45)
);

CREATE TABLE Produit (
	id_produit INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
	nom_produit VARCHAR(45),
	prix_produit DECIMAL(5,2),
	description_produit VARCHAR(1000),
	dernier_maj TIMESTAMP,
	categorie_id INT,
	CONSTRAINT fk_produit FOREIGN KEY (categorie_id) REFERENCES Categorie(id_categorie)
);

CREATE TABLE Client(
	id_client INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
	nom_client VARCHAR(45),
	prenom_client VARCHAR(45),
	email VARCHAR(45),
	rue VARCHAR(45),
	code_postal INT,
	ville VARCHAR(45),
	pays VARCHAR(45),
	telephone VARCHAR(15),
	mot_de_passe VARCHAR(45)
);

CREATE TABLE Commande_Client(
	id_commande_client INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
	montant DECIMAL(6.2),
	date_creation TIMESTAMP,
	no_confirmation INT,
	client_id INT,
	CONSTRAINT fk_commande_client FOREIGN KEY (client_id) REFERENCES Client(id_client)
);

CREATE TABLE Produit_Commande(
	commande_client_id INT,
	produit_id INT,
	quantite INT,
	CONSTRAINT fk_commande_client_produit FOREIGN KEY (commande_client_id) REFERENCES Produit(id_produit),
	CONSTRAINT fk_commande_produit FOREIGN KEY (produit_id) REFERENCES Commande_Client(id_commande_client)
);


INSERT INTO Categorie (nom_categorie) VALUES ("Livre");
INSERT INTO Categorie (nom_categorie) VALUES ("DVD");
INSERT INTO Categorie (nom_categorie) VALUES ("CD");

INSERT INTO Produit (nom_produit,prix_produit,description_produit,dernier_maj,categorie_id)
	VALUES ("Ghost Whisperer",30.0,
	"Je m'appelle Mélinda Gordon.
	Je viens de me marier, d'emménager dans une petite ville et d'ouvrir une boutique d'antiquités.
	Je pourrais être comme vous mais depuis mon enfance, j'ai découvert que je peux entrer en contact avec les morts.
	Les esprits errants comme disait ma grand-mère.
	Ceux qui ne sont pas passés de l'autre côté parce qu'ils ont des affaires à résoudre parmi les vivants et ils viennent me demander de l'aide.
	Pour raconter mon histoire, je dois vous raconter les leurs.",'2010-04-02 15:28:22',2);

INSERT INTO Produit (nom_produit,prix_produit,description_produit,dernier_maj,categorie_id)
	VALUES ("Tu ne tuera point",15.99,
	"Quand la Seconde Guerre mondiale a éclaté,
	Desmond, un jeune américain, s’est retrouvé confronté à un dilemme : comme n’importe lequel de ses compatriotes, il voulait servir son pays,
	mais la violence était incompatible avec ses croyances et ses principes moraux. Il s’opposait ne serait-ce qu’à tenir une arme et refusait d’autant
	plus de tuer. Il s’engagea tout de même dans l’infanterie comme médecin. Son refus d’infléchir ses convictions lui valut d’être rudement mené par ses
	camarades et sa hiérarchie, mais c’est armé de sa seule foi qu’il est entré dans l’enfer de la guerre pour en devenir l’un des plus grands héros.
	Lors de la bataille d’Okinawa sur l’imprenable falaise de Maeda, il a réussi à sauver des dizaines de vies seul sous le feu de l’ennemi, ramenant en sureté,
	du champ de bataille, un à un les soldats blessés. ", '2010-04-02 15:28:22',2);

INSERT INTO Produit (nom_produit,prix_produit,description_produit,dernier_maj,categorie_id)
	VALUES ("Sequestre", 9.99,
	"`Ce dimanche-l&à, Annie, agent immobilier de 32 ans, avait prévu de vendre une maison grâce à une journée portes ouvertes.
	Mais son dernier client, un homme qui prétend s'appeler David, la fait monter de force dans sa camionnette. Annie vient d'être enlevée.
	Son ravisseur l'emmène au coeur de la forêt et l'emprisonne dans une cabane. Son calvaire va commencer.",'2010-04-02 15:28:22',1);

INSERT INTO Produit (nom_produit,prix_produit,description_produit,dernier_maj,categorie_id)
	VALUES ("Ska-P", 7.99,
	"1 	Estampida 	05:32 	¡¡Que corra la voz!! \n
	2 	El Gato López 	02:45 	El vals del obrero \n
	3 	Niño Soldado 	04:00 	¡¡Que corra la voz!! \n
	4 	Planeta Eskoria 	04:36 	Planeta Eskoria \
	5 	Mestizaje 	04:33 	Planeta Eskoria
	6 	Intifada 	03:43 	¡¡Que corra la voz!!
	7 	Vals Del Obrero 	05:47 	El vals del obrero
	8 	Mis Colegas 	04:16 	¡¡Que corra la voz!!
	9 	Vergüenza 	04:25 	Planeta Eskoria
	10 	Solamente Por Pensar (En Italiano) 	03:34 	¡¡Que corra la voz!!
	11 	Romero El Madero 	04:52 	El vals del obrero
	12 	Welcome To Hell 	04:37 	¡¡Que corra la voz!!
	13 	A la Mierda 	04:07 	Planeta Eskoria
	14 	Kasposos 	06:44 	¡¡Que corra la voz!!
	15 	Paramilitar 	04:39 	Eurosis
	16 	Cannabis",'2010-04-02 15:28:22',3);

INSERT INTO Client (nom_client,prenom_client,email,rue,code_postal,ville,pays,telephone,mot_de_passe) VALUES ("Denis","Jimmy","jimmy-denis@live.fr","53 quai aux fleurs",59240,"Dunkerque","France",0652697401,"motdepasse01");
INSERT INTO Client (nom_client,prenom_client,email,rue,code_postal,ville,pays,telephone,mot_de_passe) VALUES ("Harrat","Zohra","senouci12@hotmail.fr","14 rue de la tamise",59760,"Grande-Synthe","France",0601924259,"motdepasse02");
